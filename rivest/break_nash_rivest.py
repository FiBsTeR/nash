"""
Attempt to break Nash's cryptosystem, as implemented by Prof. Rivest.
"""

__author__ = 'Justin Venezuela (jven@mit.edu)'

from nash_rivest import NashMethod

def get_nm():
  return NashMethod(6,
      [0, 5, 0, 4, 1, 6, 2, 3],
      [0, 0, 0, 1, 0, 0, 1, 1],
      [0, 6, 4, 2, 0, 1, 3, 5],
      [0, 1, 0, 1, 1, 1, 0, 0],
      [0, 1, 1, 0, 1, 1, 0, 1])

def test():
  m = [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1]
  print m
  c = get_nm().Enc(m)
  print c
  d = get_nm().Dec(c)
  print d
  assert m == d, "Ron is wrong."

def code_breakers():
  # The message to recover.
  m = [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1]
  # The ciphertext.
  c = get_nm().Enc(m)
  # The goal is to recover m from c without using m or Dec. We assume we have
  # access to Enc though: a chosen ciphertext attack.
  x = []
  for i in range(len(m)):
    if ''.join(map(str, c[:i+1])).startswith(
        ''.join(map(str, get_nm().Enc(x + [0])))):
      x.append(0)
    else:
      x.append(1)
  # gg nash
  assert m == x, 'rerere'

if __name__ == '__main__':
  #test()
  code_breakers()
