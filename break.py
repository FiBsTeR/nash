"""
Attempts to break Nash's cryptosystem!
"""

__author__ = 'Justin Venezuela (jven@mit.edu)'

from itertools import permutations
from nash import NashCryptosystem

def histogram(message_size, memory_size):
  """
  Takes in a message size and a memory size. For every message of the given
  length, constructs a histogram of ciphertexts for each possible key of the
  given size.
  """
  # The possible messages
  messages = [[1 if x & (2 ** i) != 0 else 0 for i in range(
      message_size - 1, -1, -1)] for x in range(2 ** message_size)]
  # The possible parameters for the cryptosystem
  ps = [list(l) for l in permutations([x + 1 for x in range(memory_size - 1)])]
  flips = [[True if x & (2 ** i) != 0 else False for i in range(
      memory_size - 1, -1, -1)] for x in range(2 ** memory_size)]
  # Generate histogram for each value of the cleartext
  for m in messages:
    t = [0] * (2 ** message_size)
    for p0 in ps:
      for p1 in ps:
        for flip0 in flips:
          for flip1 in flips:
            ncs = NashCryptosystem(memory_size, p0, p1, flip0, flip1)
            c = ncs.encrypt(m)
            #d = ncs.decrypt(c)
            #assert d == m, 'ruh roh'
            t[c_to_int(c)] += 1
    print t

def c_to_int(c):
  ans = 0
  for i in range(len(c)):
    ans += c[i] * (2 ** (len(c) - i - 1))
  return ans

def code_breakers(message_size, memory_size):
  """
  Generates all messages of the given length, encrypts it, and attempts to
  recreate it using only the encrypter and the ciphertext.
  """
  # The possible messages
  messages = [[1 if x & (2 ** i) != 0 else 0 for i in range(
      message_size - 1, -1, -1)] for x in range(2 ** message_size)]
  # The possible parameters for the cryptosystem
  ps = [list(l) for l in permutations([x + 1 for x in range(memory_size - 1)])]
  flips = [[True if x & (2 ** i) != 0 else False for i in range(
      memory_size - 1, -1, -1)] for x in range(2 ** memory_size)]
  # gogogo
  for p0 in ps:
    for p1 in ps:
      for flip0 in flips:
        for flip1 in flips:
          ncs = NashCryptosystem(memory_size, p0, p1, flip0, flip1)
          for m in messages:
            ncs.reset()
            c = ncs.encrypt(m)
            ans = []
            for i in range(len(c)):
              ncs.reset()
              t = ncs.encrypt(ans + [0])
              if ''.join(map(str,c[:i+1])).startswith(''.join(map(str,t))):
                ans.append(0)
              else:
                ans.append(1)
            assert ans == m, 'Failed to recover message.'

if __name__ == '__main__':
  #histogram(4, 4)
  code_breakers(4, 4)
