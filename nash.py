"""
Implementation of Nash's cryptosystem.
http://www.nsa.gov/public_info/press_room/2012/nash_exhibit.shtml
"""

__author__ = 'Justin Venezuela (jven@mit.edu)'

class InvalidInputException(Exception):
  def __init__(self, message):
    self.message = message
  def __str__(self):
    return self.message

class Retarder:
  """
  Represents a component that delays the input stream by one bit.
  """
  def __init__(self):
    """
    Constructor.
    """
    self.reset()

  def step(self, x):
    """
    Steps forward one cycle: remembers the given bit and outputs the bit in
    memory.
    """
    if x != 0 and x != 1:
      raise InvalidInputException('Invalid bit \'%s\'.' % x)
    l = self.last
    self.last = x
    return l

  def reset(self):
    """
    Reset the state of the retarder. We assume that the initial output of the
    retarder is 0, no matter the initial input.
    """
    self.last = 0

class Adder:
  """
  Represents a component that sums the two input bits mod 2.
  """
  def __init__(self):
    """
    Constructor.
    """
    pass

  def step(self, x1, x2):
    """
    Steps forward one cycle: returns the mod 2 sum of the given bits.
    """
    if x1 != 0 and x1 != 1:
      raise InvalidInputException('Invalid bit \'%s\'.' % x1)
    if x2 != 0 and x2 != 1:
      raise InvalidInputException('Invalid bit \'%s\'.' % x2)
    return (x1 + x2) % 2

class Permuter:
  """
  Represents a component that permutes the input stream in a predetermined
  (and secret!) way.
  """
  def __init__(self, size, p0, p1, flip0, flip1):
    """
    Constructor. Takes in the size of the permuter's 'memory', as well as the
    two (secret!) permutations and the (secret!) array of flips. The
    permutations must in fact be permutations of [1, ..., size - 1]. The flip
    arrays must be of the given size and consist of booleans, representing
    whether outgoing bits from that bit of memory should be flipped, for both
    values of the deciding bit.
    """
    if size < 1:
      raise InvalidInputException('Memory size must be positive.')
    if (not Permuter._is_permutation(p0, size - 1) or
        not Permuter._is_permutation(p1, size - 1)):
      raise InvalidInputException('Permutations must be cycles of the given '
          'length, less one.')
    if len(flip0) != size or len(flip1) != size:
      raise InvalidInputException('Flip arrays must be an array of booleans '
          'of the given size.')
    self.size = size
    self.p0 = [0] + p0
    self.p1 = [0] + p1
    self.flip0 = flip0
    self.flip1 = flip1
    self.reset()

  def step(self, x):
    """
    Steps forward one cycle: permutes the memory according to one of the
    permutations (depending on the value of x).
    """
    # Get the appropriate permutation and flip array depending on the value of
    # the decider bit.
    if x == 0:
      p = self.p0
      flip = self.flip0
    elif x == 1:
      p = self.p1
      flip = self.flip1
    else:
      raise InvalidInputException('Invalid bit \'%s\'.' % x)
    # Move the decider bit into memory, then dittle the bits.
    for idx in p:
      if flip[idx]:
        self.memory[idx], x = x, 1 - self.memory[idx]
      else:
        self.memory[idx], x = x, self.memory[idx]
    # All bits have been dittled, x now holds the value to output.
    return x

  def reset(self):
    """
    Reset the state of the permuter. We assume that the initial state of the
    permuter is that all bits of memory are 0.
    """
    self.memory = [0] * self.size

  @staticmethod
  def _is_permutation(l, n):
    """
    Returns if the given list l is a permutation of [1, ..., n].
    """
    if len(l) != n:
      return False
    t = [False] * n
    for e in l:
      if e < 1 or e > n or t[e - 1]:
        return False
      t[e - 1] = True
    for e in t:
      if not e:
        return False
    return True

class NashEncryptionSystem:
  """
  Represents an implementation of Nash's encryption system.
  """
  def __init__(self, size, p0, p1, flip0, flip1):
    """
    Constructor. Takes in parameters for the Permuter (see specs).
    """
    self.my_adder = Adder()
    self.my_permuter = Permuter(size, p0, p1, flip0, flip1)
    self.reset()

  def step(self, x):
    """
    Steps forward one cycle: takes in a bit of input, adds it with the output
    of the permuter in the previous cycle, and outputs the sum. The sum is also
    pushed into the permuter. The permuter outputs a bit which is saved for
    input into the adder in the next cycle.
    """
    output = self.my_adder.step(x, self.last_permuter_output)
    self.last_permuter_output = self.my_permuter.step(output)
    return output

  def process(self, l):
    """
    Takes a sequence of bits and steps through each one. Outputs the sequence
    of bits output by each step.
    """
    return [self.step(x) for x in l]

  def reset(self):
    """
    Reset the encrypter to the initial state.
    """
    self.last_permuter_output = 0
    self.my_permuter.reset()

class NashDecryptionSystem:
  """
  Represents an implementation of Nash's decryption system.
  """
  def __init__(self, size, p0, p1, flip0, flip1):
    """
    Constructor. Takes in parameters for the Permuter (see specs).
    """
    self.my_adder = Adder()
    self.my_permuter = Permuter(size, p0, p1, flip0, flip1)
    self.my_retarder = Retarder()
    self.reset()

  def step(self, x):
    """
    Steps forward one cycle: takes in a bit of input, adds it with the output
    of the permuter in the previous cycle, and outputs the sum. The input of
    the previous cycle is also pushed into the permuter. The permuter outputs
    a bit which is saved for input into the adder in the next cycle.
    """
    output = self.my_adder.step(x, self.last_permuter_output)
    # The retarder is in fact inherent to our implementation of the permuter,
    # so we do not need it.
    self.last_permuter_output = self.my_permuter.step(x)
    return output

  def process(self, l):
    """
    Takes a sequence of bits and steps through each one. Outputs the sequence
    of bits output by each step.
    """
    return [self.step(x) for x in l]

  def reset(self):
    """
    Reset the decrypter to the initial state.
    """
    self.last_permuter_output = 0
    self.my_permuter.reset()

class NashCryptosystem:
  """
  Represents an implementation of Nash's cryptosystem.
  """
  def __init__(self, size, p0, p1, flip0, flip1):
    """
    Constructor. Takes in parameters for the Permuter (see specs).
    """
    self.encrypter = NashEncryptionSystem(size, p0, p1, flip0, flip1)
    self.decrypter = NashDecryptionSystem(size, p0, p1, flip0, flip1)

  def encrypt(self, cleartext):
    """
    Encrypts a sequence of bits.
    """
    return self.encrypter.process(cleartext)

  def decrypt(self, ciphertext):
    """
    Decrypts a sequence of bits.
    """
    return self.decrypter.process(ciphertext)

  def reset(self):
    """
    Reset the cryptosystem to the initial state.
    """
    self.encrypter.reset()
    self.decrypter.reset()
