"""
Tests for Nash cryptosystem implementation.
"""

__author__ = 'Justin Venezuela (jven@mit.edu)'

import unittest
from nash import Adder
from nash import InvalidInputException
from nash import NashCryptosystem
from nash import Permuter
from nash import Retarder

class TestRetarder(unittest.TestCase):

  def setUp(self):
    # We make a new retarder for each case to clear state.
    pass

  def testRetarderNormalBehavior(self):
    input_streams = [
        [0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1],
        [1, 1, 1],
        [0],
        [1],
        []
    ]
    expected_outputs = [
        [0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0],
        [0, 1, 1],
        [0],
        [0],
        []
    ]
    for idx in range(len(input_streams)):
      my_retarder = Retarder()
      output = []
      for bit in input_streams[idx]:
        output.append(my_retarder.step(bit))
      self.assertEqual(expected_outputs[idx], output)

class TestAdder(unittest.TestCase):

  def setUp(self):
    # We use the same adder each time since Adders have no state.
    self.my_adder = Adder()

  def testAdderNormalBehavior(self):
    # Test the four cases.
    self.assertEqual(0, self.my_adder.step(0, 0))
    self.assertEqual(1, self.my_adder.step(0, 1))
    self.assertEqual(1, self.my_adder.step(1, 0))
    self.assertEqual(0, self.my_adder.step(1, 1))

  def testAdderInvalidInputs(self):
    # Test that an invalid bit throws an exception.
    self.assertRaises(InvalidInputException, lambda:self.my_adder.step(2, 0))
    self.assertRaises(InvalidInputException, lambda:self.my_adder.step(0, -1))
    self.assertRaises(InvalidInputException, lambda:self.my_adder.step(5.2, 3))

class TestPermuter(unittest.TestCase):

  def setUp(self):
    pass

  def testIsPermutationValid(self):
    valid_permutations = [
        ([1 + x for x in range(3)], 3),
        ([1 + x for x in range(4)], 4),
        ([1 + x for x in range(8)], 8),
        ([1 + x for x in range(200)], 200),
        ([1, 3, 4, 2, 5], 5),
        ([2, 5, 1, 4, 3], 5),
        ([5, 4, 3, 2, 1], 5),
        ([1, 2], 2),
        ([2, 1], 2),
        ([1], 1),
        ([], 0)
    ]
    for (l, n) in valid_permutations:
      self.assertTrue(Permuter._is_permutation(l, n))

  def testIsPermutationInvalid(self):
    invalid_permutations = [
        # l has too many elements
        (range(4), 3),
        ([0, 2, 3, 2, 4], 5),
        # l has too few elements
        (range(3), 4),
        ([0], 2),
        # l has enough elements and repetitions
        ([1, 4, 0, 3, 2, 5], 5),
        # l has invalid elements
        ([-1, 0, 1], 2),
        ([-1, 0, 1], 3),
        ([0, 1, 2, 3.5], 4),
        # random [] test
        ([], 1)
    ]
    for (l, n) in invalid_permutations:
      self.assertFalse(Permuter._is_permutation(l, n))

  def testPermuterFixed(self):
    # Test a permuter with trivial permutations and no flips
    size = 6
    my_permuter = Permuter(
        size, [1 + x for x in range(size - 1)],
        [1 + x for x in range(size - 1)], [False] * size, [False] * size)
    input_stream = [1, 1, 1, 0, 0, 0, 0, 0, 1, 0]
    output_stream = []
    for bit in input_stream:
      output_stream.append(my_permuter.step(bit))
    self.assertEqual([0] * size + input_stream[:len(input_stream) - size],
        output_stream)

class TestNashCryptosystem(unittest.TestCase):

  def setUp(self):
    pass

  def testNormalBehavior(self):
    ncs_params = [
        (4, [1, 3, 2], [3, 1, 2],
            [True, False, True, False], [True, True, True, True]),
        (3, [2, 1], [2, 1],
            [False, False, False], [True, True, True]),
        (7, [1, 3, 2, 5, 6, 4], [5, 4, 2, 1, 6, 3],
            [False, True, True, False, True, False, True],
            [True, True, True, False, True, True, True])
    ]
    messages = [
        [0, 0, 0, 0, 0, 0, 0, 0],
        [1, 1, 0, 0 ,1, 0, 1, 1, 1, 1, 0, 1, 0],
        [1, 1],
        [0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0],
        [1, 1, 1, 1, 1, 0, 0, 0, 0],
        []
    ]
    for cleartext in messages:
      for args in ncs_params:
        ncs = NashCryptosystem(args[0], args[1], args[2], args[3], args[4])
        ciphertext = ncs.encrypt(cleartext)
        output = ncs.decrypt(ciphertext)
        self.assertEqual(cleartext, output)

if __name__ == '__main__':
  unittest.main()
